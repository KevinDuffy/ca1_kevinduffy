#ifndef ARRAY_H
#define ARRAY_H

#include <iostream>

template<class Datatype>
class OrderedArray
{
private:
	Datatype* m_array;
	int m_size;
	int m_numElements;
	double m_growSize;
public:
	//constructor which sets up the size of the array and sets
	//grow size to be 1.5 times the existing size.
	OrderedArray(int p_size)
	{
		// allocate enough memory for the array
		m_array = new Datatype[p_size];
		m_size = p_size;
		m_growSize = 1.5;
		m_numElements = 0;
	}

	~OrderedArray()
	{
		if (m_array != 0)
		{
			delete[] m_array;
		}
		m_array = 0;
	}

	//getter for size of array
	int Size()
	{
		return m_size;
	}

	//allow the user to input a value to set the array to
	//no protection for deletion of data if new array is smaller
	//than old array
	void Resize(int p_size)
	{
		//create new array
		Datatype* newArray = new Datatype[p_size];
		//if the new array wasnt allocated, then just return 
		if (newArray == 0)
		{
			std::cout << "The call to new failed and returned 0" << std::endl;
			return;
		}

		//determine if making the array bigger or smaller
		int min;
		if (p_size < m_size)
		{
			min = p_size;
		}
		else
		{
			min = m_size;
		}
		//copy all elements from old array to new array

		for (int index = 0; index < min; index++)
		{
			newArray[index] = m_array[index];
		}

		//set size of new array
		m_size = p_size;

		// delete old array
		if (m_array != 0)
		{
			delete[] m_array;
		}

		//make the array pointer point to the new array location
		m_array = newArray;

		//set the functions array pointer back to zero - clean-up process
		newArray = 0;
	}

	// if the Array is full and a new item is added this method will automatically
	//create a new array with 50% more space
	void Grow()
	{
		int newSize = m_size*m_growSize;
		//create new array with 50% more room
		Datatype* newArray = new Datatype[newSize];
		//if the new array wasnt allocated, then just return 
		if (newArray == 0)
		{
			std::cout << "The call to new failed and returned 0" << std::endl;
			return;
		}

		//copy all elements from old array to new array
		for (int index = 0; index < m_size; index++)
		{
			newArray[index] = m_array[index];
		}

		//set size of new array
		
		m_size = newSize;

		// delete old array
		if (m_array != 0)
		{
			delete[] m_array;
		}

		//make the array pointer point to the new array location
		m_array = newArray;

		//set the functions array pointer back to zero - clean-up process
		newArray = 0;
	}

	// override [] so that it works as you would expect with an array
	Datatype& operator[](int p_index)
	{
		return m_array[p_index];
	}

		// conversion operator to convert an array into a pointer
	// so we can pass our array into functions
	operator Datatype*()
	{
		return m_array;
	}

	// insert a new element in the correct order
	void Push(Datatype p_item)
	{
		//resize if the array isnt big enough
		if(m_numElements == m_size)
			Grow();
		//find where the element should go
		int index = m_numElements;
		for(int i = 0 ;i < m_numElements ; i++)
		{
			if(p_item <= m_array[i])
		{
			index = i;
			break;
		}
		}


		//move the elements up after the entry point
		for(int j = m_numElements; j >= index; j--)
		{
			m_array[j] = m_array[j-1];
		}
		//insert the element
		m_array[index] = p_item;
		//increase the element count
		m_numElements++;
	}
	

	//not entirely happy here
	//seems ridiculously onerous for what needs to be done
	//take the last element off the array.
	//Complexity
	//O(0)
	void Pop()
	{
		m_numElements--;
	}

	// this remove function keeps the order of the array
	// complexity: O(n)
	void Remove(int p_index)
	{
		int index;
		// move everything after index down 1
		for (index = p_index + 1; index < m_size; index++)
		{
			m_array[index - 1] = m_array[index];
		}
	}

	//Complexity
	//O(log(n))
	//http://mycodinglab.com/binary-search-algorithm-c/
	//used binary search found above and adjusted so only requested value is necessary
	//binary search returns middle instance if more than one isntance of the element being searched
	//for exists in the array
	int Binary_Search(int p_item)
	{
		int index;
		int first = 0;
		//if there is nothing in the array return -1
		if (first > m_numElements)
		{
			index = -1;
		}
		//check if the requested element is on the midpoint, below the midpoint, or above the midpoint
		else
		{
			int mid = (first + m_numElements)/2;
			//call the binary search with narrower constraints
			if (p_item == m_array[mid])	
				index = mid;
			else if (p_item < m_array[mid])
				index = Binary_Search( m_array, first, mid-1, p_item);
			else
				index = Binary_Search( m_array, mid+1, m_numElements, p_item);
 
		} 
		return index;
	}
	//binary search as found on the website
	int Binary_Search(int array[],int first,int last, int search_key)
	{
		int index;
 
		if (first > last)
			index = -1;
 
		else
		{
			int mid = (first + last)/2;
 
			if (search_key == array[mid])
			{
				index = mid;
			}
			else if (search_key < array[mid])
			{
				index = Binary_Search(array,first, mid-1, search_key);
			}
			else
			{
				index = Binary_Search(array, mid+1, last, search_key);
			}
		} 
			return index;
	}

	//Complexity
	//O(n)
	int Search(int p_value)
	{
		for(int i = 0; i < m_numElements; ++i)
		{
			if(m_array[i] == p_value)
				return i;
		}
		return -1;
	}

	//Complexity
	//O(n)
	//print function to print out all elements of array
	void Print()
	{
		for(int i = 0; i < m_numElements; i++)
		{
			cout << m_array[i] << endl;
		}
		cout << endl;
	}

	//Clear function reduces the number of elements in the array to zero
	//this basically clears your array
	//Complexity
	//O(0)
	void Clear()
	{
		m_numElements = 0;
	}

	//WriteOrdered writes out a binary file of your sorted array to a userdefined address
	//Complexity
	//O(n)
	bool WriteOrdered(const char* p_filename)
	{
		const char* type = "wb";
		FILE *outfile = fopen(p_filename, type);
		int written = 0;
		
		if(outfile == 0)
			return false;

		written = fwrite(m_array, sizeof(Datatype), m_numElements, outfile);
		fclose(outfile);

		if(written != m_size)
			return false;
		
		return true;
	}


	bool ReadUnordered(const char* p_filename)
	{
		const char* type = "rb";
		FILE* infile = 0;
		int read = 0;
		infile = fopen(p_filename, type);
		if(infile == 0)
			return false;

		read = fread(m_array, sizeof(Datatype), m_size, infile);
		fclose(infile);
		if(read != m_size)
			return false;

		return true;
	}

	};
#endif // !ARRAY_H
